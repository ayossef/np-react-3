
# Install Dev Env.

## Update the OS packages

```bash
sudo apt update
```

## Install node 
### Instal node via Ubuntu packages
it is an easy way. It does't provide latest versions
```bash
sudo apt install nodejs
```

### Install node via Node package
```bash
# add support of node servers to your apt tool
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt install nodejs
```

### verify node installation
```bash
node --version
npm --version
```

## Install VS Code
Downloading code deb package from web site
```bash
dpkg -i code-----.deb
```
Install some VS Code plugins

## Install create-react-app (gobally) 
```bash
sudo npm install -g create-react-app
```

## Create your first app
```bash
npx create-react-app react-one
```

## Go to project and start it
```bash
cd react-one
npm start
```
