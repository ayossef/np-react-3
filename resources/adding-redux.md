# Adding Redux Support to out project

## Install Redux Packages
We have to install two main packages
- The core package (redux - @redux/toolkit) 
- The bridge package (react-redux)

```bash
npm install @reduxjs/toolkit react-redux
```

## Code Elements


### Creating the Store


### Define the state, Reducres and Actions


### Provide the store
Provider is used to define the scope of using the store. 
Warpping the [App] component with the Provider tag, accsing the store everywhere.


### Import useDispatch in writing compnents


### Import useSelector in reading Components
